import React from 'react';
import { Center, Box, Button, ScrollView } from 'native-base';
import DescripcionFechas from '../../../componentes/DescripcionFechas';
import TituloModulo from '../../../componentes/TituloModulo';
import SubtituloModulo from '../../../componentes/SubtituloModulo';

function SuscripcionActiva() {
  return (
    <ScrollView w='100%' h='80'>
      <Center w='100%'>
        <Box safeArea p='2' py='8' w='90%' maxW='290'>
          <TituloModulo
            name='Suscripción'
            colorBase='warning.100'
            colorFondo='warning.300'
            nombreIonicos='card'
          ></TituloModulo>
          <Box mt='5' mb='5' />
          <SubtituloModulo texto='Plan activo' mt='5'></SubtituloModulo>
          <DescripcionFechas
            titulo='Plan básico'
            texto='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed
              iaculis auctor dolor a fermentum. Proin non augue tempus,
              vestibulum nulla non, dictum nunc.'
            finaliza='00-00-xxxx'
          ></DescripcionFechas>

          <Button
            mt='2'
            title='Cambiar plan'
            onPress={() => console.log('username, pass')}
          >
            Cambiar plan
          </Button>
        </Box>
      </Center>
    </ScrollView>
  );
}

export default SuscripcionActiva;
