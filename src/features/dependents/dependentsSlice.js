import AsyncStorage from "@react-native-async-storage/async-storage";
import { createSlice } from "@reduxjs/toolkit";
import BASE_URL_BACKEND from "../../config/baseUrl.js";

const initialState = [];

const dependentsSlice = createSlice({
  name: "dependets",
  initialState,
  reducers: {
    renderDependants: (state, action) => {
      action.payload.map((dep) => {
        const resultado = state.find(
          (dato) => dato.idDependent === dep.idDependent
        );

        if (resultado) {
        } else {
          state.push(dep);
        }
      });
      console.log("_____________________new data___________________________-");
      console.log(action.payload);
    },
  },
});

export const { renderDependants } = dependentsSlice.actions;
export default dependentsSlice.reducer;
