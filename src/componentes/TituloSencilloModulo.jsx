import React from 'react';
import { Heading } from 'native-base';

function TituloSencilloModulo(props) {
  return (
    <Heading
      mt='4'
      size='lg'
      fontWeight='600'
      color='black'
      alignSelf='center'
      _dark={{
        color: 'warmGray.500',
      }}
    >
      {props.name}
    </Heading>
  );
}

export default TituloSencilloModulo;
