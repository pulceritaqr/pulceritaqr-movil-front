import React from 'react';
import { Text, Center, Box, VStack, Button, ScrollView } from 'native-base';
function Prueba(props) {
  return (
    <ScrollView w='100%' h='80'>
      <Center w='100%'>
        <Box safeArea p='2' py='8' w='90%' maxW='290'>
          <VStack space={5} mt='5'>
            <Text>Saltar intro</Text>
            <Button
              shadow={2}
              onPress={() => {
                props.navigation.navigate('Login');
              }}
            >
              Click me to login
            </Button>
            <Button
              shadow={2}
              onPress={() => {
                props.navigation.navigate('Register');
              }}
            >
              Click me to register
            </Button>

            <Button
              shadow={2}
              onPress={() => {
                props.navigation.navigate('Home');
              }}
            >
              Click me to skip
            </Button>
          </VStack>
        </Box>
      </Center>
    </ScrollView>
  );
}

export default Prueba;
