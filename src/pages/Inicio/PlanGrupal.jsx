import React from 'react';
import {
  Center,
  Box,
  Button,
  ScrollView,
  VStack,
  HStack,
  Spacer,
  Text,
  Flex,
  Icon,
} from 'native-base';
import SubtituloPrincipal from '../../componentes/SubtituloPrincipal';
import ImgHolder from '../../componentes/ImgHolder';
import DescripcionSencilla from '../../componentes/DescripcionSencilla';
import BtnNext from '../../componentes/BtnNext';
import { Ionicons } from '@expo/vector-icons';
import SchoolAnimation from '../../componentes/SchoolAnimation';


function PlanGrupal(props) {
  return (
    <ScrollView w='100%' h='80'>
      <Center w='100%'>
        <Box safeArea p='2' py='8' w='90%' maxW='290'>
          <VStack space={3} mt='5'>
            <Flex direction='row-reverse'>
              <Text
                alignSelf='center'
                color='indigo.500'
                fontWeight='medium'
                fontSize='md'
                onPress={() => {
                  props.navigation.navigate('Login');
                }}
              >
                Saltar intro
              </Text>
            </Flex>
            <SubtituloPrincipal texto='Plan Grupal' />
            <Spacer mt='2' mb='2' />
            <SchoolAnimation />
            <DescripcionSencilla texto='Este plan esta dirigido a escuelas, guarderias, asilos entre otros. Incluye la inscripción de 5 personas o más y una pulsera por persona, el pago inicial es de 500 pesos mexicanos y 50 por persona inscrita.' />
            <HStack space={2} mt='5' justifyContent='center'>
              <Button size='sm' variant='ghost' borderRadius={100}>
                <Icon
                  as={Ionicons}
                  name='radio-button-off-outline'
                  size='15'
                  alignSelf='center'
                  margin='auto'
                  color='indigo.500'
                  onPress={() => {
                    props.navigation.navigate('InfoServicio');
                  }}
                />
              </Button>
              <Button size='sm' variant='ghost' borderRadius={100}>
                <Icon
                  as={Ionicons}
                  name='radio-button-off-outline'
                  size='15'
                  alignSelf='center'
                  margin='auto'
                  color='indigo.500'
                  onPress={() => {
                    props.navigation.navigate('PlanFamiliar');
                  }}
                />
              </Button>
              <Button size='sm' variant='ghost' borderRadius={100}>
                <Icon
                  as={Ionicons}
                  name='radio-button-on-outline'
                  size='15'
                  alignSelf='center'
                  margin='auto'
                  color='indigo.500'
                  onPress={() => {
                    props.navigation.navigate('PlanGrupal');
                  }}
                />
              </Button>
              <Button size='sm' variant='ghost' borderRadius={100}>
                <Icon
                  as={Ionicons}
                  name='radio-button-off-outline'
                  size='15'
                  alignSelf='center'
                  margin='auto'
                  color='indigo.500'
                  onPress={() => {
                    props.navigation.navigate('InfoEnvios');
                  }}
                />
              </Button>
            </HStack>
          </VStack>
        </Box>
      </Center>
    </ScrollView>
  );
}

export default PlanGrupal;
