import React from 'react';
import { Heading } from 'native-base';

function SubtituloModulo(props) {
  return (
    <>
      <Heading
        mt='1'
        _dark={{
          color: 'warmGray.200',
        }}
        color='coolGray.800'
        fontWeight='medium'
        size='md'
      >
        {props.texto}
      </Heading>
    </>
  );
}

export default SubtituloModulo;
