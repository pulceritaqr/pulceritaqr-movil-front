import React from 'react';
import { Heading, Center, Box, HStack, Icon, ZStack } from 'native-base';
import { Ionicons, FontAwesome } from '@expo/vector-icons';

function TituloDefault(props) {
  return (
    <HStack space={10} justifyContent='center' mt='-5' mb='5'>
      <Center h='35'>
        <Box>
          <ZStack ml={-50}>
            <Box bg={props.colorFondo} size='16' rounded='md' shadow={3} />
            <Box
              bg={props.colorBase}
              mt='5'
              ml='5'
              size='16'
              rounded='lg'
              shadow={5}
            >
              <Icon
                as={Ionicons}
                name={props.nombreIonicos}
                size='10'
                alignSelf='center'
                margin='auto'
                color={props.colorFondo}
              />
            </Box>
          </ZStack>
        </Box>
      </Center>

      <Heading
        mt='8'
        ml='5'
        size='lg'
        fontWeight='600'
        color='black'
        alignSelf='center'
        _dark={{
          color: 'warmGray.50',
        }}
      >
        {props.name}
      </Heading>
    </HStack>
  );
}

export default TituloDefault;
