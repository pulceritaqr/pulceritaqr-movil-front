import React from 'react';
import { Center, Box, ScrollView, VStack, HStack } from 'native-base';
import TarjetaInicio from '../../componentes/TarjetaInicio';
import TituloDefault from '../../componentes/TituloDefault';
import SubtituloPrincipal from '../../componentes/SubtituloPrincipal';

function HomeScreen(props) {
  const irModulo = (nombrePantalla) => {
    props.navigation.navigate(nombrePantalla);
  };

  return (
    <ScrollView w='100%' h='80'>
      <Center w='100%'>
        <Box safeArea p='2' py='8' w='90%' maxW='290'>
          <TituloDefault texto='Bienvenido a PulceritaQR' />
          <SubtituloPrincipal texto='Por que te quiero, te cuido ❤️ ' />

          <VStack space={3} mt='5'>
            <HStack mt='10' justifyContent='space-between'>
              <TarjetaInicio
                name='Configuración'
                colorBase='green.100'
                colorFondo='green.500'
                nombreIonicos='settings-outline'
                irModulo={irModulo}
                nombrePantalla='Configuracion'
              />
              <TarjetaInicio
                name='Suscripción'
                colorBase='warning.100'
                colorFondo='warning.300'
                nombreIonicos='card-outline'
                irModulo={irModulo}
                nombrePantalla='Planes'
              />
              <TarjetaInicio
                name='Mi información'
                colorBase='blue.100'
                colorFondo='blue.500'
                nombreIonicos='person'
                irModulo={irModulo}
                nombrePantalla='MiInfo'
              />
            </HStack>
            <HStack mt='6' justifyContent='space-between'>
              <TarjetaInicio
                name='Mis personas'
                colorBase='indigo.100'
                colorFondo='indigo.400'
                nombreIonicos='information-circle'
                irModulo={irModulo}
                nombrePantalla='PersonasLista'
              />
              <TarjetaInicio
                name='Cerrar sesión'
                colorBase='coolGray.200'
                colorFondo='coolGray.500'
                nombreIonicos='log-out-outline'
                irModulo={irModulo}
                nombrePantalla='Home'
              />
              <TarjetaInicio
                name='Soporte'
                colorBase='red.200'
                colorFondo='red.400'
                nombreIonicos='headset-outline'
                irModulo={irModulo}
                nombrePantalla='Soporte'
              />
            </HStack>
          </VStack>
        </Box>
      </Center>
    </ScrollView>
  );
}

export default HomeScreen;
