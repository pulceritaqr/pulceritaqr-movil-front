import React from "react";
import {
  Image,
  Box,
  Center,
  AspectRatio,
  Icon,
  Button,
  VStack,
  Pressable,
  Badge,
  Flex,
  HStack,
  Text,
  Spacer,
} from "native-base";
import QRCode from 'react-native-qrcode-svg';
import { Ionicons, FontAwesome } from "@expo/vector-icons";
import BASE_URL_BACKEND from "../config/baseUrl";

function PersonaRow(props) {
  let imagenURL = props.imagenURL;
  let nombrePersona = props.nombrePersona;
  let urlDep;
  if(props.urlQR != null ) urlDep = BASE_URL_BACKEND + props.urlQR;
  const navigation = props.navigation;
  const fun = props.fun;
  const goToDetalle = () => {
    props.personaEditar(5);
  };

  return (
    <>
      <Pressable mt="2" mb="1" onPress={goToDetalle}>
        {({ isHovered, isFocused, isPressed }) => {
          return (
            <Box
              maxW="96"
              borderWidth="1"
              borderColor="coolGray.300"
              shadow="3"
              bg={
                isPressed
                  ? "coolGray.200"
                  : isHovered
                    ? "coolGray.200"
                    : "coolGray.100"
              }
              p="2"
              rounded="8"
              style={{
                transform: [
                  {
                    scale: isPressed ? 0.96 : 1,
                  },
                ],
              }}
            >
              <HStack space="3" alignItems="center">
                <Box
                  maxW="80"
                  rounded="lg"
                  overflow="hidden"
                  borderColor="coolGray.200"
                  borderWidth="1"
                >
                  <Box size={60}>
                    <AspectRatio w="100%" ratio={3 / 3}>
                      <Image
                        source={{
                          uri: imagenURL,
                        }}
                        alt="ProfileImg"
                      />
                    </AspectRatio>
                    <Button
                      size="sm"
                      variant="ghost"
                      bg="coolGray.500"
                      alignSelf="flex-end"
                      position="absolute"
                      bottom="0"
                      px="1.5"
                      py="1.3"
                    >
                      <Icon
                        as={Ionicons}
                        name="create-outline"
                        size="15"
                        alignSelf="center"
                        margin="auto"
                        color="light.100"
                      />
                    </Button>
                  </Box>
                </Box>
                <Box textAlign="justify" w={"100%"} m={1}>
                  <Text textAlign="justify" fontSize="xs" fontWeight="medium">
                    {nombrePersona}
                  </Text>
                  {urlDep != null ?
                    <QRCode
                      value={urlDep}
                    /> : <></>}
                </Box>
              </HStack>
            </Box>
          );
        }}
      </Pressable>
    </>
  );
}

export default PersonaRow;
