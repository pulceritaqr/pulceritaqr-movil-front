import React from 'react';
import { Image, Box, AspectRatio, Icon, Button } from 'native-base';
import { Ionicons, FontAwesome } from '@expo/vector-icons';

function ImgProfile(props) {
  let imagenURL = props.imagenURL;

  return (
    <Box alignItems='center'>
      <Box
        mt='5'
        maxW='80'
        rounded='lg'
        overflow='hidden'
        borderColor='coolGray.200'
        borderWidth='1'
        _dark={{
          borderColor: 'coolGray.600',
          backgroundColor: 'gray.700',
        }}
        _web={{
          shadow: 2,
          borderWidth: 0,
        }}
        _light={{
          backgroundColor: 'gray.50',
        }}
      >
        <Box size={120}>
          <AspectRatio w='100%' ratio={3 / 3}>
            <Image
              source={{
                uri: imagenURL,
              }}
              alt='image'
            />
          </AspectRatio>

          <Button
            size='sm'
            variant='ghost'
            bg='indigo.500'
            alignSelf='flex-end'
            position='absolute'
            bottom='0'
            px='3'
            py='3.0'
          >
            <Icon
              as={Ionicons}
              name='camera-outline'
              size='15'
              alignSelf='center'
              margin='auto'
              color='light.100'
            />
          </Button>
        </Box>
      </Box>
    </Box>
  );
}

export default ImgProfile;
