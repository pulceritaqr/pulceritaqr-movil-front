import { configureStore } from "@reduxjs/toolkit";

import dependentsReducer from "../features/dependents/dependentsSlice";

export const store = configureStore({
  reducer: { dependents: dependentsReducer },
});
