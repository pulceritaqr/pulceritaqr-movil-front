import React from 'react';
import {
  Text,
  Box,
  VStack,
  Switch,
  HStack,
  Divider,
  Heading,
  Icon,
} from 'native-base';
import { Ionicons, FontAwesome } from '@expo/vector-icons';

function DescripcionFechas(props) {
  return (
    <>
      <VStack space={4} mt='3'>
        <HStack alignItems='space-between' space={4}>
          <Box w={['80%', '80%', '40']} h='full'>
            <Heading
              mt='1'
              _dark={{
                color: 'warmGray.200',
              }}
              color='coolGray.600'
              fontWeight='medium'
              size='xs'
            >
              {props.titulo}
            </Heading>
            <Text>{props.texto}</Text>
          </Box>
          <Box w={['20%', '20%', '40']} alignSelf='center'>
            <Icon
              as={Ionicons}
              name='bookmark-sharp'
              size='25'
              alignSelf='center'
              margin='auto'
              color='Indigo.200'
            />
          </Box>
        </HStack>

        <HStack justifyContent='space-between' space={4}>
          <Text>Finaliza</Text>
          <Text fontWeight='medium'>{props.finaliza}</Text>
        </HStack>
        <Divider
          my='1'
          _light={{
            bg: 'muted.300',
          }}
          _dark={{
            bg: 'muted.50',
          }}
        />
      </VStack>
    </>
  );
}

export default DescripcionFechas;
