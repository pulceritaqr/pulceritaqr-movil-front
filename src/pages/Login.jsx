import { Alert } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useToast } from "react-native-toast-notifications";
import {
  Heading,
  Text,
  Center,
  ScrollView,
  Box,
  VStack,
  Input,
  Button,
  FormControl,
  Link,
  HStack,
  Spacer,
} from "native-base";
import React, { useState } from "react";
import authController from "../controllers/authController";
import BtnRedesSociales from "../componentes/BtnRedesSociales";
import TituloDefault from "../componentes/TituloDefault";

function Login(props) {
  const toast = useToast();
  const [username, setUsername] = useState("");
  const [pass, setPass] = useState("");

  async function reqLogin(username, pass) {
    if (!username || !pass)
      return Alert.alert("Username and password are required");
    const res = await authController.authLogin(username, pass);
    if (res.message) {
      Alert.alert(res.message);
    } else {
      //const user = await AsyncStorage.getItem("user");
      //console.log("user", user);
      //props.setUser(res);
      toast.show("Bienvenido", {
        type: "success",
        placement: "bottom",
        duration: 4000,
        offset: 30,
        animationType: "slide-in",
      });
      props.navigation.navigate("Home");
    }
  }

  return (
    <ScrollView w="100%" h="80">
      <Center w="100%">
        <Box safeArea p="2" py="8" w="90%" maxW="290">
          <TituloDefault texto="PulceritaQR" />
          <Spacer mt="2" />
          <Heading
            mt="1"
            _dark={{
              color: "warmGray.200",
            }}
            color="coolGray.600"
            fontWeight="medium"
            size="xs"
          >
            Ingresa a tu cuenta
          </Heading>

          <VStack space={3} mt="5">
            <FormControl>
              <FormControl.Label>Email</FormControl.Label>
              <Input
                variant="outline"
                placeholder="Username"
                onChangeText={setUsername}
              />
            </FormControl>
            <FormControl>
              <FormControl.Label>Password</FormControl.Label>
              <Input
                variant="outline"
                onChangeText={setPass}
                placeholder="Password"
                secureTextEntry={true}
                value={pass}
              />
              <Link
                _text={{
                  fontSize: "xs",
                  fontWeight: "500",
                  color: "indigo.500",
                }}
                alignSelf="flex-end"
                mt="1"
              >
                Forget Password?
              </Link>
            </FormControl>
            <Button
              mt="2"
              colorScheme="indigo"
              onPress={() => reqLogin(username, pass)}
            >
              Login
            </Button>

            <HStack mt="6" justifyContent="center">
              <Text
                fontSize="sm"
                color="coolGray.600"
                _dark={{
                  color: "warmGray.200",
                }}
              >
                - También puedes ingresar con: -
              </Text>
            </HStack>

            <HStack mt="6" justifyContent="space-between">
              <BtnRedesSociales
                nombreIonicos="logo-google"
                colorFondo="red.500"
              />

              <BtnRedesSociales
                nombreIonicos="logo-facebook"
                colorFondo="blue.600"
              />

              <BtnRedesSociales
                nombreIonicos="logo-twitter"
                colorFondo="blue.400"
              />
            </HStack>

            <HStack mt="6" justifyContent="center">
              <Text
                fontSize="sm"
                color="coolGray.600"
                _dark={{
                  color: "warmGray.200",
                }}
              >
                Soy un usuario nuevo.{" "}
              </Text>
              <Text
                color="indigo.500"
                fontWeight="medium"
                fontSize="sm"
                onPress={() => {
                  props.navigation.navigate("Register");
                }}
              >
                Sign Up
              </Text>
            </HStack>
          </VStack>
        </Box>
      </Center>
    </ScrollView>
  );
}

export default Login;
