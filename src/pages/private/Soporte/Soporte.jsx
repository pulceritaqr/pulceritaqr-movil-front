import React from 'react';
import {
  Center,
  Box,
  ScrollView,
  VStack,
  HStack,
  FormControl,
  Input,
  Spacer,
  Flex,
  Button,
} from 'native-base';
import TituloModulo from '../../../componentes/TituloModulo';
import SubtituloModulo from '../../../componentes/SubtituloModulo';
import DescripcionSencilla from '../../../componentes/DescripcionSencilla';

function Soporte(props) {
  const goToScreen = () => {
    props.navigation.navigate('EnvioForm');
  };

  return (
    <ScrollView w='100%' h='80'>
      <Center w='100%'>
        <Box safeArea p='2' py='8' w='90%' maxW='290'>
          <TituloModulo
            name='Soporte'
            colorBase='red.200'
            colorFondo='red.400'
            nombreIonicos='headset-outline'
          ></TituloModulo>
          <Box mt='5' mb='5' />
          <SubtituloModulo
            texto='Información de envio sobre tu pulcera'
            mt='5'
          />

          <Spacer mt='4' mb='3' />
          <SubtituloModulo texto='Mi dirección de envios' />
          <DescripcionSencilla texto='Estado nombre, municipio nombre, colonia nombre, direccion detallada' />
          <Flex direction='row-reverse'>
            <Button
              onPress={goToScreen}
              variant='subtle'
              size='xs'
              height='10'
              width='30%'
              colorScheme='indigo'
            >
              Modificar
            </Button>
          </Flex>
          <Spacer mt='4' mb='3' />

          <SubtituloModulo texto='Detalles de mi envio' />
          <DescripcionSencilla texto='Último estatus de envio' />
          <Flex direction='row-reverse'>
            <Button
              variant='subtle'
              size='xs'
              height='10'
              width='30%'
              colorScheme='indigo'
            >
              Llamanos
            </Button>
          </Flex>
          <Spacer mt='4' mb='3' />

          <SubtituloModulo texto='Contactanos' />
          <DescripcionSencilla texto='Cualquier duda o comentario sobre nuestro servicio ' />
          <Flex direction='row-reverse'>
            <Button
              variant='subtle'
              size='xs'
              height='10'
              width='30%'
              colorScheme='indigo'
            >
              Llamanos
            </Button>
          </Flex>
        </Box>
      </Center>
    </ScrollView>
  );
}

export default Soporte;
