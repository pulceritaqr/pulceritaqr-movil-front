import React from 'react';
import { Button, HStack, Icon } from 'native-base';
import { Ionicons } from '@expo/vector-icons';

function BtnNext(props) {
  return (
    <HStack space={2} mt='5' justifyContent='center'>
      <Button size='sm' variant='ghost'>
        <Icon
          as={Ionicons}
          name='radio-button-off-outline'
          size='15'
          alignSelf='center'
          margin='auto'
          color='indigo.500'
        />
      </Button>
      <Button size='sm' variant='ghost'>
        <Icon
          as={Ionicons}
          name='radio-button-off-outline'
          size='15'
          alignSelf='center'
          margin='auto'
          color='indigo.500'
        />
      </Button>
      <Button size='sm' variant='ghost'>
        <Icon
          as={Ionicons}
          name='radio-button-off-outline'
          size='15'
          alignSelf='center'
          margin='auto'
          color='indigo.500'
        />
      </Button>
    </HStack>
  );
}

export default BtnNext;
