import React from 'react';
import {
  Center,
  Box,
  ScrollView,
  VStack,
  Button,
  FormControl,
  Input,
} from 'native-base';
import TituloModulo from '../../../componentes/TituloModulo';
import SubtituloModulo from '../../../componentes/SubtituloModulo';

function DireccionEnvio() {
  return (
    <ScrollView w='100%' h='80'>
      <Center w='100%'>
        <Box safeArea p='2' py='8' w='90%' maxW='290'>
          <Box mt='5' mb='5' />
          <SubtituloModulo
            texto='Información de envio sobre tu pulcera'
            mt='5'
          />

          <VStack space={3} mt='5'>
            <FormControl>
              <FormControl.Label>Nombre</FormControl.Label>
              <Input variant='outline' placeholder='Nombre (s)' />
            </FormControl>

            <FormControl>
              <FormControl.Label>Apellido (s)</FormControl.Label>
              <Input variant='outline' placeholder='Apellido (s)' />
            </FormControl>

            <FormControl>
              <FormControl.Label>Estado /Provincia</FormControl.Label>
              <Input variant='outline' placeholder='Estado' />
            </FormControl>

            <FormControl>
              <FormControl.Label>Municipio</FormControl.Label>
              <Input variant='outline' placeholder='Municipio' />
            </FormControl>

            <FormControl>
              <FormControl.Label>Dirección</FormControl.Label>
              <Input variant='outline' placeholder='Dirección' />
            </FormControl>

            <FormControl>
              <FormControl.Label>Código postal</FormControl.Label>
              <Input variant='outline' placeholder='Código postal' />
            </FormControl>

            <FormControl>
              <FormControl.Label>Referencias</FormControl.Label>
              <Input variant='outline' placeholder='Referencias' />
            </FormControl>

            <Button
              mt='2'
              colorScheme='indigo'
              onPress={() => console.log('pressed')}
            >
              Aceptar
            </Button>
          </VStack>
          <Box mt='5' mb='5' />
        </Box>
      </Center>
    </ScrollView>
  );
}

export default DireccionEnvio;
