import React from 'react';
import {
  Text,
  Box,
  VStack,
  Button,
  Switch,
  HStack,
  Divider,
  Heading,
} from 'native-base';

function DescripcionChk(props) {
  let titulo = props.titulo;
  let texto = props.texto;

  return (
    <>
      <VStack space={4} mt='3'>
        <HStack alignItems='space-between' space={4}>
          <Box w={['80%', '80%', '40']} h='full'>
            <Heading
              mt='1'
              _dark={{
                color: 'warmGray.200',
              }}
              color='coolGray.600'
              fontWeight='medium'
              size='xs'
            >
              {titulo}
            </Heading>
            <Text>{texto}</Text>
          </Box>
          <Box w={['20%', '20%', '40']} alignSelf='center'>
            <Switch size='md' onTrackColor='indigo.500' />
          </Box>
        </HStack>
        <Divider
          my='1'
          _light={{
            bg: 'muted.300',
          }}
          _dark={{
            bg: 'muted.50',
          }}
        />
      </VStack>
    </>
  );
}

export default DescripcionChk;
