import React from 'react';
import { Text, View } from 'react-native';
import { Button } from 'native-base';
import { createDrawerNavigator } from '@react-navigation/drawer';
import PruebaInterna from './PruebaInterna';

import Configuracion from './Configuracion/Configuracion';
import MiInfo from './MiInfo/MiInfoForm';
import Payment from './Payment/PaymentOpt';
import Planes from './Suscripcion/Planes';
import SuscripcionActiva from './Suscripcion/SuscripcionActiva';
import PersonasLista from './Personas/PersonasLista';

import HomeScreen from './HomeScreen';
import InfoServicio from '../Inicio/InfoServicio';
import VistaCivil from '../VistaCivil';
import Soporte from './Soporte/Soporte';
/**
 * 
import { createDrawerNavigator } from '@react-navigation/drawer';
  const Drawer = createDrawerNavigator();
    <Drawer.Navigator initialRouteName='Home'>
      <Drawer.Screen name='Home' component={PruebaInterna} />
      <Drawer.Screen name='Notifications' component={PruebaInterna} />
    </Drawer.Navigator>
 */

function SideBar(props) {
  const Drawer = createDrawerNavigator();
  return (
    <Drawer.Navigator initialRouteName='Home'>
      <Drawer.Screen name='Home' component={HomeScreen} />
      <Drawer.Screen name='Mi información' component={MiInfo} />
      <Drawer.Screen name='Mis personas' component={PersonasLista} />
      <Drawer.Screen
        name='Suscripción Activa (Temporal)'
        component={SuscripcionActiva}
      />
      <Drawer.Screen name='Planes(Temporal)' component={Planes} />
      <Drawer.Screen name='Configuración' component={Configuracion} />
      <Drawer.Screen name='VistaCivil' component={VistaCivil} />
      <Drawer.Screen name='Soporte' component={Soporte} />
    </Drawer.Navigator>
  );
}

export default SideBar;
