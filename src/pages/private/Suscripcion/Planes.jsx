import React from 'react';
import { Center, Box, Spacer, Text, Flex, ScrollView } from 'native-base';
import TarjetaDescripcion from '../../../componentes/TarjetaDescripcion';
import TituloModulo from '../../../componentes/TituloModulo';
import SubtituloModulo from '../../../componentes/SubtituloModulo';

function SuscripcionInicial(props) {
  const irModulo = () => {
    props.navigation.navigate('Payment');
  };

  return (
    <ScrollView w='100%' h='80'>
      <Center w='100%'>
        <Box safeArea p='2' py='8' w='90%' maxW='290'>
          <TituloModulo
            name='Suscripción'
            colorBase='warning.100'
            colorFondo='warning.300'
            nombreIonicos='card'
          ></TituloModulo>
          <Spacer mt='5' mb='5' />
          <SubtituloModulo texto='Selecciona un plan' mt='5'></SubtituloModulo>
          <Spacer mt='3' mb='3' />
          <TarjetaDescripcion
            titulo='Plan familiar'
            texto='Este plan incluye la inscripción de 1 a 4 personas y una pulsera por persona inscrita.'
            finaliza='12 meses'
            precio='Pago inicial 100.00'
            cos='Por persona inscrita 70.00'
            irModulo={irModulo}
          />

          <Spacer mt='3' mb='3' />
          <TarjetaDescripcion
            titulo='Plan grupal'
            texto='El plan incluye la inscripción de 5 personas o más y una pulsera por persona inscrita.'
            finaliza='12 meses'
            precio='Pago inicial 500.00'
            cos='Por persona inscrita 50.00'
            irModulo={irModulo}
          />
        </Box>
      </Center>
    </ScrollView>
  );
}

export default SuscripcionInicial;
