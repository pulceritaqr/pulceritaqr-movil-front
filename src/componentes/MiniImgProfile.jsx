import React from 'react';
import { Center, Image, Box, AspectRatio, Icon, Button } from 'native-base';
import { Ionicons, FontAwesome } from '@expo/vector-icons';

function MiniImgProfile(props) {
  return (
    <Box alignItems='center'>
      <Box
        mt='1'
        maxW='80'
        rounded='lg'
        overflow='hidden'
        borderColor='coolGray.200'
        borderWidth='1'
        _dark={{
          borderColor: 'coolGray.600',
          backgroundColor: 'gray.700',
        }}
        _web={{
          shadow: 2,
          borderWidth: 0,
        }}
        _light={{
          backgroundColor: 'gray.50',
        }}
      >
        <Box size={60}>
          <AspectRatio w='100%' ratio={3 / 3}>
            <Image
              source={{
                uri: 'https://www.holidify.com/images/cmsuploads/compressed/Bangalore_citycover_20190613234056.jpg',
              }}
              alt='image'
            />
          </AspectRatio>

          <Button
            size='sm'
            variant='ghost'
            bg='indigo.500'
            alignSelf='flex-end'
            position='absolute'
            bottom='0'
            px='2'
            py='2.0'
          >
            <Icon
              as={Ionicons}
              name='create-outline'
              size='15'
              alignSelf='center'
              margin='auto'
              color='light.100'
            />
          </Button>
        </Box>
      </Box>
    </Box>
  );
}

export default MiniImgProfile;
