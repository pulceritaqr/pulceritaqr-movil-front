import React from 'react';
import {
  Center,
  Box,
  ScrollView,
  VStack,
  Input,
  Button,
  FormControl,
  Spacer,
} from 'native-base';

import ImgProfile from '../../../componentes/ImgProfile';
import TituloModulo from '../../../componentes/TituloModulo';

function MiInfoForm() {
  return (
    <ScrollView w='100%' h='80'>
      <Center w='100%'>
        <Box safeArea p='2' py='8' w='90%' maxW='290'>
          <TituloModulo
            name='Mi información'
            colorBase='blue.100'
            colorFondo='blue.500'
            nombreIonicos='person'
          />
          <Spacer mt='5' />
          <ImgProfile imagenURL='https://media.revistavanityfair.es/photos/60e859a276b409bfd6cb93ce/1:1/w_1350,h_1350,c_limit/23457.jpg' />
          <VStack space={3} mt='5'>
            <FormControl>
              <FormControl.Label>Nombre (s)</FormControl.Label>
              <Input variant='outline' placeholder='Nombre (s)' />
            </FormControl>

            <FormControl>
              <FormControl.Label>Primer apellido</FormControl.Label>
              <Input variant='outline' placeholder='Primer apellido' />
            </FormControl>

            <FormControl>
              <FormControl.Label>Segundo apellido</FormControl.Label>
              <Input variant='outline' placeholder='Segundo apellido' />
            </FormControl>

            <FormControl>
              <FormControl.Label>Nombre de usuario</FormControl.Label>
              <Input variant='outline' placeholder='Nombre de usuario' />
            </FormControl>

            <FormControl>
              <FormControl.Label>Genero</FormControl.Label>
              <Input variant='outline' placeholder='Genero' />
            </FormControl>

            <FormControl>
              <FormControl.Label>Dirección</FormControl.Label>
              <Input variant='outline' placeholder='Dirección' />
            </FormControl>

            <FormControl>
              <FormControl.Label>Email</FormControl.Label>
              <Input variant='outline' placeholder='Email' />
            </FormControl>

            <FormControl>
              <FormControl.Label>Password</FormControl.Label>
              <Input
                variant='outline'
                placeholder='Password'
                secureTextEntry={true}
                value={'pass'}
              />
            </FormControl>

            <FormControl>
              <FormControl.Label>Repetir password</FormControl.Label>
              <Input
                variant='outline'
                placeholder='Repetir password'
                secureTextEntry={true}
                value={'pass'}
              />
            </FormControl>

            <Button
              mt='2'
              colorScheme='indigo'
              onPress={() => console.log('pressed')}
            >
              Aceptar
            </Button>
          </VStack>
        </Box>
      </Center>
    </ScrollView>
  );
}

export default MiInfoForm;
