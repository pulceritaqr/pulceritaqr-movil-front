import React, { useState } from "react";
import { useToast } from "react-native-toast-notifications";
import {
  Center,
  Box,
  ScrollView,
  VStack,
  Input,
  Button,
  FormControl,
  TextArea,
  Select,
} from "native-base";

import ImgProfile from "../../../componentes/ImgProfile";
import TituloSencilloModulo from "../../../componentes/TituloSencilloModulo";
import personController from "../../../controllers/personController";
import { useDispatch, useSelector } from "react-redux";
import { renderDependants } from "../../../features/dependents/dependentsSlice";

function PersonaForm({ route, navigation }) {
  const dispatch = useDispatch();
  const toast = useToast();
  const { idPerson, screen } = route.params;
  const [gender, setGender] = useState("");
  const [name, setName] = useState("");
  const [ap1, setAp1] = useState("");
  const [ap2, setAp2] = useState("");
  const [address, setAddress] = useState("");
  const [specialC, setSpecialC] = useState("");

  async function handleSubmit(gender, name, ap1, ap2, address, specialC) {
    const res = await personController.addPerson(
      gender,
      name,
      ap1,
      ap2,
      address,
      specialC
    );

    if (res.message) {
      let para = [];
      para.push(res.data);
      dispatch(renderDependants(para));
      console.log(res);
      toast.show(res.message, {
        type: "success",
        placement: "bottom",
        duration: 4000,
        offset: 30,
        animationType: "slide-in",
      });
    }
  }

  return (
    <ScrollView w="100%" h="80">
      <Center w="100%">
        <Box safeArea p="2" py="8" w="90%" maxW="290">
          <TituloSencilloModulo name={screen}></TituloSencilloModulo>
          <ImgProfile></ImgProfile>

          <VStack space={3} mt="5">
            <FormControl>
              <FormControl.Label>Nombre</FormControl.Label>
              <Input
                variant="outline"
                placeholder="Nombre"
                onChangeText={(nombre) => setName(nombre)}
              />
            </FormControl>

            <FormControl>
              <FormControl.Label>Apellido paterno</FormControl.Label>
              <Input
                variant="outline"
                placeholder="Apellido paterno"
                onChangeText={(apellido1) => setAp1(apellido1)}
              />
            </FormControl>

            <FormControl>
              <FormControl.Label>Apellido materno</FormControl.Label>
              <Input
                variant="outline"
                onChangeText={(apellido2) => setAp2(apellido2)}
                placeholder="Apellido materno"
              />
            </FormControl>

            <FormControl>
              <FormControl.Label>Direccion</FormControl.Label>
              <Input
                variant="outline"
                onChangeText={(direccion) => setAddress(direccion)}
                placeholder="Direccion"
              />
            </FormControl>

            <FormControl>
              <FormControl.Label>Cuidados especiales</FormControl.Label>
              <TextArea
                h={20}
                placeholder="Cuidados especiales"
                w="100%"
                maxW="300"
                onChangeText={(cuidados) => setSpecialC(cuidados)}
              />
            </FormControl>
            <FormControl>
              <FormControl.Label>Genero</FormControl.Label>
              <Select
                selectedValue={gender}
                minWidth="200"
                accessibilityLabel="Genero"
                placeholder="Genero"
                _selectedItem={{
                  bg: "teal.600",
                }}
                mt={1}
                onValueChange={(genero) => setGender(genero)}
              >
                <Select.Item label="Masculino" value="Masculino" />
                <Select.Item label="Femenino" value="Femenino" />
              </Select>
            </FormControl>

            <Button
              mt="2"
              colorScheme="indigo"
              onPress={() =>
                handleSubmit(gender, name, ap1, ap2, address, specialC)
              }
            >
              Aceptar
            </Button>
          </VStack>
        </Box>
      </Center>
    </ScrollView>
  );
}

export default PersonaForm;
