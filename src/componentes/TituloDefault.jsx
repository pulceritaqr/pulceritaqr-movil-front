import React from 'react';
import { Heading } from 'native-base';

function TituloDefault(props) {
  return (
    <>
      <Heading
        size='lg'
        fontWeight='600'
        color='indigo.800'
        alignSelf='center'
        mt='5'
        _dark={{
          color: 'warmGray.50',
        }}
      >
        {props.texto}
      </Heading>
    </>
  );
}

export default TituloDefault;
