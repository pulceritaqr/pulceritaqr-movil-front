import React from 'react';
import { Center, Box, VStack, Button, ScrollView } from 'native-base';
import TituloDefault from '../componentes/TituloDefault';
import SubtituloPrincipal from '../componentes/SubtituloPrincipal';
import DescripcionSencilla from '../componentes/DescripcionSencilla';
import ImgHolder from '../componentes/ImgHolder';

function VistaCivil(props) {
  return (
    <ScrollView w='100%' h='80'>
      <Center w='100%'>
        <Box safeArea p='2' py='8' w='90%' maxW='290'>
          <TituloDefault texto='Bienvenido a PulceritaQR' />
          <SubtituloPrincipal texto='Por que te quiero, te cuido ❤️ ' />
          <DescripcionSencilla texto='Explicacion.rar' />
          <VStack space={3} mt='5'>
            <ImgHolder />
          </VStack>
        </Box>
      </Center>
    </ScrollView>
  );
}

export default VistaCivil;
