import React from 'react';
import {
  Center,
  Box,
  Button,
  ScrollView,
  VStack,
  HStack,
  Spacer,
  Flex,
  Text,
  Icon,
} from 'native-base';
import TituloSencilloModulo from '../../componentes/TituloSencilloModulo';
import ImgHolder from '../../componentes/ImgHolder';
import DescripcionSencilla from '../../componentes/DescripcionSencilla';
import BtnNext from '../../componentes/BtnNext';
import { Ionicons } from '@expo/vector-icons';
import DeliveryAnimation from '../../componentes/DeliveryAnimation';
import SubtituloPrincipal from '../../componentes/SubtituloPrincipal';

function InfoEnvios(props) {
  return (
    <ScrollView w='100%' h='80'>
      <Center w='100%'>
        <Box safeArea p='2' py='8' w='90%' maxW='290'>
          <VStack space={3} mt='1'>
            <SubtituloPrincipal texto='Información sobre envios' />
            <DescripcionSencilla texto='Envíos a través de mercado libre, DHL y correos de México. Al realizar tu pedido te hacemos llegar el estatus del mismo por medio de WhatsApp, cuando enviamos tu pedido te compartimos el detalle de tu compra, puedes ver dónde está el paquete en cada momento. De esta forma, podrás saber cuándo llegará tu paquete y si surge algún imprevisto. 
Contamos con una dirección de correo electrónico y número WhatsApp a la que los clientes pueden enviar dudas o comentarios sobre nuestro servicio.
' />
            <DeliveryAnimation />
            <Button
              mt='2'
              colorScheme='indigo'
              onPress={() => {
                props.navigation.navigate('Login');
              }}
            >
              Aceptar
            </Button>
            <HStack space={2} mt='5' justifyContent='center'>
              <Button size='sm' variant='ghost' borderRadius={100}>
                <Icon
                  as={Ionicons}
                  name='radio-button-off-outline'
                  size='15'
                  alignSelf='center'
                  margin='auto'
                  color='indigo.500'
                  onPress={() => {
                    props.navigation.navigate('InfoServicio');
                  }}
                />
              </Button>
              <Button size='sm' variant='ghost' borderRadius={100}>
                <Icon
                  as={Ionicons}
                  name='radio-button-off-outline'
                  size='15'
                  alignSelf='center'
                  margin='auto'
                  color='indigo.500'
                  onPress={() => {
                    props.navigation.navigate('PlanFamiliar');
                  }}
                />
              </Button>
              <Button size='sm' variant='ghost' borderRadius={100}>
                <Icon
                  as={Ionicons}
                  name='radio-button-off-outline'
                  size='15'
                  alignSelf='center'
                  margin='auto'
                  color='indigo.500'
                  onPress={() => {
                    props.navigation.navigate('PlanGrupal');
                  }}
                />
              </Button>
              <Button size='sm' variant='ghost' borderRadius={100}>
                <Icon
                  as={Ionicons}
                  name='radio-button-on-outline'
                  size='15'
                  alignSelf='center'
                  margin='auto'
                  color='indigo.500'
                  onPress={() => {
                    props.navigation.navigate('InfoEnvios');
                  }}
                />
              </Button>
            </HStack>
          </VStack>
        </Box>
      </Center>
    </ScrollView>
  );
}

export default InfoEnvios;
