import React from 'react';
import { Heading } from 'native-base';

function SubtituloDefault(props) {
  return (
    <>
      <Heading
        mt='1'
        _dark={{
          color: 'warmGray.200',
        }}
        color='coolGray.600'
        fontWeight='medium'
        size='xs'
      >
        {props.texto}
      </Heading>
    </>
  );
}

export default SubtituloDefault;
