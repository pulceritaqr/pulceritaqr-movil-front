import React from "react";
import { Alert } from "react-native";
import { useToast } from "react-native-toast-notifications";
import { useEffect, useState } from "react";
import {
  Heading,
  Text,
  Center,
  ScrollView,
  Box,
  VStack,
  Input,
  Button,
  FormControl,
  HStack,
} from "native-base";

import registerController from "../controllers/registerController";
import BtnRedesSociales from "../componentes/BtnRedesSociales";
import TituloDefault from "../componentes/TituloDefault";

function Register(props) {
  const toast = useToast();
  const [username, setUsername] = useState({});
  const [email, setEmail] = useState({});
  const [password, setPassword] = useState({});
  const [password2, setPassword2] = useState({});

  async function handleSubmit(username, email, password) {
    if (
      email !== null &&
      email !== undefined &&
      email.length >= 6 &&
      password !== null &&
      password !== undefined &&
      password.length >= 6
    ) {
      if (password === password2) {
        const res = await registerController.register(
          username,
          email,
          password
        );
        if (res.message) {
          console.log(res);
          toast.show(res.message, {
            type: "success",
            placement: "bottom",
            duration: 4000,
            offset: 30,
            animationType: "slide-in",
          });
        }
      } else {
        toast.show("Las contraseñas no coinciden", {
          type: "warning",
          placement: "bottom",
          duration: 4000,
          offset: 30,
          animationType: "slide-in",
        });
      }
    } else {
      toast.show("Por favor, llena todos los campos", {
        type: "warning",
        placement: "bottom",
        duration: 4000,
        offset: 30,
        animationType: "slide-in",
      });
    }
  }

  return (
    <ScrollView w="100%" h="80">
      <Center w="100%">
        <Box safeArea p="2" py="8" w="90%" maxW="290">
          <TituloDefault texto="PulceritaQR"></TituloDefault>
          <Heading
            mt="1"
            _dark={{
              color: "warmGray.200",
            }}
            color="coolGray.600"
            fontWeight="medium"
            size="xs"
          >
            Crea tu cuenta
          </Heading>

          <VStack space={3} mt="5">
            <FormControl>
              <FormControl.Label>Email</FormControl.Label>
              <Input
                variant="outline"
                placeholder="Email"
                onChangeText={(email) => {
                  setEmail(email);
                  setUsername(email);
                }}
              />
            </FormControl>

            <FormControl>
              <FormControl.Label>Password</FormControl.Label>
              <Input
                variant="outline"
                onChangeText={(password) => setPassword(password)}
                placeholder="Password"
                secureTextEntry={true}
              />
            </FormControl>

            <FormControl>
              <FormControl.Label>Confirma tu password</FormControl.Label>
              <Input
                variant="outline"
                onChangeText={(password2) => setPassword2(password2)}
                placeholder="Password"
                secureTextEntry={true}
              />
            </FormControl>
            <Button
              mt="2"
              colorScheme="indigo"
              onPress={() => handleSubmit(username, email, password)}
            >
              Aceptar
            </Button>

            <HStack mt="6" justifyContent="center">
              <Text
                fontSize="sm"
                color="coolGray.600"
                _dark={{
                  color: "warmGray.200",
                }}
              >
                - También puedes registrarte con: -
              </Text>
            </HStack>

            <HStack mt="6" justifyContent="space-between">
              <BtnRedesSociales
                nombreIonicos="logo-google"
                colorFondo="red.500"
              />

              <BtnRedesSociales
                nombreIonicos="logo-facebook"
                colorFondo="blue.600"
              />

              <BtnRedesSociales
                nombreIonicos="logo-twitter"
                colorFondo="blue.400"
              />
            </HStack>

            <HStack mt="6" justifyContent="center">
              <Text
                fontSize="sm"
                color="coolGray.600"
                _dark={{
                  color: "warmGray.200",
                }}
              >
                Ya tengo una cuenta.{" "}
              </Text>
              <Text
                color="indigo.500"
                fontWeight="medium"
                fontSize="sm"
                onPress={() => {
                  props.navigation.navigate("Login");
                }}
              >
                Login
              </Text>
            </HStack>
          </VStack>
        </Box>
      </Center>
    </ScrollView>
  );
}

export default Register;
