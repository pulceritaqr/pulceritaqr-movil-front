import React from 'react';
import { Platform } from 'react-native';
import { Center, Box, Pressable, Icon, Text } from 'native-base';
import { Ionicons, FontAwesome } from '@expo/vector-icons';
function TarjetaInicio(props) {
  let colorBase = props.colorBase;
  let nombreIonicos = props.nombreIonicos;
  let colorFondo = props.colorFondo;
  let nombreTarjeta = props.name;
  const goToScreen = () => {
    props.irModulo(props.nombrePantalla);
  };

  return (
    <Pressable onPress={goToScreen}>
      {({ isHovered, isFocused, isPressed }) => {
        return (
          <Box
            maxW='96'
            borderWidth='1'
            borderColor='coolGray.100'
            shadow='3'
            bg={
              isPressed
                ? 'coolGray.100'
                : isHovered
                ? 'coolGray.100'
                : 'coolGray.50'
            }
            rounded='8'
            style={{
              transform: [
                {
                  scale: isPressed ? 0.96 : 1,
                },
              ],
            }}
          >
            <Center w='80px' h='80px' bg={colorBase} rounded='5'>
              <Box
                _text={{
                  fontWeight: 'medium',
                  fontSize: 'xs',
                  color: 'black',
                }}
              >
                <Icon
                  as={Ionicons}
                  name={nombreIonicos}
                  size='10'
                  alignSelf='center'
                  margin='auto'
                  color={colorFondo}
                />
                {nombreTarjeta}
              </Box>
            </Center>
          </Box>
        );
      }}
    </Pressable>
  );
}

export default TarjetaInicio;
