import React from 'react';
import { Text, Box } from 'native-base';

function DescripcionSencilla(props) {
  return (
    <>
      <Box w={['100%', '100%', '40']} mt='4' mb='4'>
        <Text>{props.texto}</Text>
      </Box>
    </>
  );
}

export default DescripcionSencilla;
