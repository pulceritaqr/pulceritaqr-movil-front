import React from 'react';
import {
  Text,
  Box,
  VStack,
  HStack,
  Heading,
  Pressable,
  Center,
  Icon,
} from 'native-base';
import { Ionicons, FontAwesome } from '@expo/vector-icons';
function TarjetaDescripcion(props) {
  const goToScreen = () => {
    props.irModulo();
  };

  return (
    <Pressable onPress={goToScreen}>
      {({ isHovered, isFocused, isPressed }) => {
        return (
          <Box
            maxW='96'
            borderWidth='1'
            borderColor='coolGray.100'
            shadow='3'
            bg={
              isPressed
                ? 'coolGray.100'
                : isHovered
                ? 'coolGray.100'
                : 'coolGray.50'
            }
            rounded='8'
            p='5'
            style={{
              transform: [
                {
                  scale: isPressed ? 0.96 : 1,
                },
              ],
            }}
          >
            <VStack space={4}>
              <HStack alignItems='space-between' space={4}>
                <Box w={['85%', '85%', '40']} h='full'>
                  <Heading
                    mt='1'
                    mb='2'
                    _dark={{
                      color: 'warmGray.200',
                    }}
                    color='Indigo.600'
                    fontWeight='bold'
                    fontSize={14}
                    alignSelf='center'
                  >
                    {props.titulo}
                  </Heading>
                  <Text>{props.texto}</Text>
                </Box>
                <Box
                  w={['15%', '15%', '40']}
                  alignSelf='center'
                  justifyContent='center'
                >
                  <Icon
                    onPress={goToScreen}
                    as={Ionicons}
                    name='checkbox-outline'
                    size='25'
                    alignSelf='center'
                    margin='auto'
                    color='Green.200'
                  />
                </Box>
              </HStack>
              <Heading
                mt='1'
                mb='2'
                _dark={{
                  color: 'warmGray.200',
                }}
                color='Indigo.600'
                fontWeight='bold'
                fontSize={14}
                alignSelf='center'
              >
                {props.precio} MNX
              </Heading>

              <Heading
                mt='1'
                mb='2'
                _dark={{
                  color: 'warmGray.200',
                }}
                color='Indigo.600'
                fontWeight='bold'
                fontSize={14}
                alignSelf='center'
              >
                {props.cos} MNX
              </Heading>
              <HStack justifyContent='space-between' space={4}>
                <Text>Duración</Text>
                <Text fontWeight='medium'>{props.finaliza}</Text>
              </HStack>
            </VStack>
          </Box>
        );
      }}
    </Pressable>
  );
}

export default TarjetaDescripcion;
