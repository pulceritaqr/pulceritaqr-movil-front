import BASE_URL_BACKEND from "../config/baseUrl.js";

const register = async (username, email, password) => {
  try {
    const res = await fetch(BASE_URL_BACKEND + "api/auth/register", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: username,
        email: email,
        role: [],
        password: password,
      }),
    });
    const data = await res.json();
    console.log("data", JSON.stringify(data));
    return data;
  } catch (err) {
    console.log(err);
    return err;
  }
};

const registerController = {
  register,
};

export default registerController;
