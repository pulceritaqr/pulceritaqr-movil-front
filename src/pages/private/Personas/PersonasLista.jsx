import React, { useEffect, useState } from "react";
import BASE_URL_BACKEND from "../../../config/baseUrl.js";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {
  Center,
  Box,
  ScrollView,
  Spacer,
  VStack,
  Text,
  Button,
} from "native-base";
import TituloModulo from "../../../componentes/TituloModulo";
import SubtituloModulo from "../../../componentes/SubtituloModulo";
import PersonaRow from "../../../componentes/PersonaRow";
import { useDispatch, useSelector } from "react-redux";
import { renderDependants } from "../../../features/dependents/dependentsSlice";
//props.navigation.navigate('Home')
function PersonasLista(props) {
  const deps = useSelector((state) => state.dependents);
  const dispatch = useDispatch();
  console.log(deps);

  const fetchData = async () => {
    const user = JSON.parse(await AsyncStorage.getItem("user"));
    try {
      const res = await fetch(
        BASE_URL_BACKEND + "api/my-dependents/" + user.id,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + user.token,
          },
        }
      );
      const persons = await res.json();
      //console.log("dataaaaaaaaaaaaaaaaaa", JSON.stringify(persons));
      //console.log("personassssssssss", persons.data);
      dispatch(renderDependants(persons.data));
    } catch (err) {
      console.log(err);
      return err;
    }
  };

  const personaEditar = (itemId) => {
    props.navigation.navigate("PersonaForm", {
      idPerson: itemId,
      screen: "Editar Persona",
    });
  };

  const personaAgregar = () => {
    props.navigation.navigate("PersonaForm", {
      idPerson: null,
      screen: "Agregar Persona",
    });
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <ScrollView w="100%" h="80">
      <Center w="100%">
        <Box safeArea p="2" py="8" w="90%" maxW="290">
          <TituloModulo
            name="Mis personas"
            colorBase="indigo.100"
            colorFondo="indigo.400"
            nombreIonicos="information-circle"
          />
          <Spacer mt="3" mb="2" />
          <SubtituloModulo texto="Tus registros" mt="5"></SubtituloModulo>
          <Spacer mt="2" mb="2" />
          <VStack>
            {deps.map((dp) => {
              return (
                <PersonaRow
                  nombrePersona={
                    dp.name + " " + dp.lastName + " " + dp.secondLastName
                  }
                  imagenURL="https://www.thefamouspeople.com/profiles/images/porfirio-daz-2.jpg"
                  personaEditar={personaEditar}
                  urlQR={dp.urlQR}
                />
              );
            })}
            <PersonaRow
              nombrePersona="Agregar nueva persona"
              imagenURL="https://www.iconsdb.com/icons/preview/gray/plus-7-xxl.png"
              personaEditar={personaAgregar}
            />
          </VStack>
        </Box>
      </Center>
    </ScrollView>
  );
}

export default PersonasLista;
