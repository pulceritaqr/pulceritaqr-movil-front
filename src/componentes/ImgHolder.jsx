import React from 'react';
import { Image, Box, AspectRatio } from 'native-base';

function ImgHolder(props) {
  return (
    <Box alignItems='center'>
      <Box
        alignItems='center'
        maxW='80'
        rounded='lg'
        overflow='hidden'
        borderColor='coolGray.200'
      >
        <AspectRatio w='100%' ratio={3 / 3} size={180}>
          <Image
            source={{
              uri: 'https://www.holidify.com/images/cmsuploads/compressed/Bangalore_citycover_20190613234056.jpg',
            }}
            alt='image'
          />
        </AspectRatio>
      </Box>
    </Box>
  );
}

export default ImgHolder;
