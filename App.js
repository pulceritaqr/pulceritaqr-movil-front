import React from "react";
import Main from "./src/pages/Main";
import { NativeBaseProvider } from "native-base";
import { ToastProvider } from "react-native-toast-notifications";
import { store } from "./src/app/store";
import { Provider } from "react-redux";
const App = () => {
  return (
    <Provider store={store}>
      <ToastProvider>
        <NativeBaseProvider>
          <Main />
        </NativeBaseProvider>
      </ToastProvider>
    </Provider>
  );
};

export default App;
