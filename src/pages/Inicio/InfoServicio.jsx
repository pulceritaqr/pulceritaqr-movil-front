import React from 'react';
import {
  Center,
  Box,
  Button,
  ScrollView,
  VStack,
  HStack,
  Text,
  Icon,
  Flex,
} from 'native-base';
import TituloDefault from '../../componentes/TituloDefault';
import SubtituloPrincipal from '../../componentes/SubtituloPrincipal';
import ImgHolder from '../../componentes/ImgHolder';
import DescripcionSencilla from '../../componentes/DescripcionSencilla';
import BtnNext from '../../componentes/BtnNext';
import { Ionicons } from '@expo/vector-icons';
import SimpleAnimation from '../../componentes/WavingAnimation';

function InfoServicio(props) {
  return (
    <ScrollView w='100%' h='80'>
      <Center w='100%'>
        <Box safeArea p='2' py='8' w='90%' maxW='290'>
          <VStack space={3} mt='5'>
            <Flex direction='row-reverse'>
              <Text
                alignSelf='center'
                color='indigo.500'
                fontWeight='medium'
                fontSize='md'
                onPress={() => {
                  props.navigation.navigate('Login');
                }}
              >
                Saltar intro
              </Text>
            </Flex>
            <SimpleAnimation />
            <TituloDefault texto='Bienvenido a PulceritaQR'></TituloDefault>
            <SubtituloPrincipal texto='Por que te quiero, te cuido ❤️ ' />
            <DescripcionSencilla texto='PulceritaQR te ofrece una pulsera con identificador que permite la localización de una persona vulnerable además de registrar y consultar los datos de contacto, fotografía, entre otros a través de un código QR en la pulsera. Al escanear el código los datos podrán ser visualizados por la persona que le brinde ayuda al portador en caso de una emergencia o extravió, además se enviará un correo de alerta al cuidador con la ubicación de donde fue escaneado el código. Lo que nos facilitara localizar a nuestra persona vulnerable en caso de que se halla extraviado.' />
            <HStack space={2} mt='5' justifyContent='center'>
              <Button size='sm' variant='ghost' borderRadius={100}>
                <Icon
                  as={Ionicons}
                  name='radio-button-on-outline'
                  size='15'
                  alignSelf='center'
                  margin='auto'
                  color='indigo.500'
                  onPress={() => {
                    props.navigation.navigate('InfoServicio');
                  }}
                />
              </Button>
              <Button size='sm' variant='ghost' borderRadius={100}>
                <Icon
                  as={Ionicons}
                  name='radio-button-off-outline'
                  size='15'
                  alignSelf='center'
                  margin='auto'
                  color='indigo.500'
                  onPress={() => {
                    props.navigation.navigate('PlanFamiliar');
                  }}
                />
              </Button>
              <Button size='sm' variant='ghost' borderRadius={100}>
                <Icon
                  as={Ionicons}
                  name='radio-button-off-outline'
                  size='15'
                  alignSelf='center'
                  margin='auto'
                  color='indigo.500'
                  onPress={() => {
                    props.navigation.navigate('PlanGrupal');
                  }}
                />
              </Button>
              <Button size='sm' variant='ghost' borderRadius={100}>
                <Icon
                  as={Ionicons}
                  name='radio-button-off-outline'
                  size='15'
                  alignSelf='center'
                  margin='auto'
                  color='indigo.500'
                  onPress={() => {
                    props.navigation.navigate('InfoEnvios');
                  }}
                />
              </Button>
            </HStack>
          </VStack>
        </Box>
        <VStack space={5} mt='5'>
          <Text>Saltar intro</Text>
          <Button
            shadow={2}
            onPress={() => {
              props.navigation.navigate('Login');
            }}
          >
            Click me to login
          </Button>
          <Button
            shadow={2}
            onPress={() => {
              props.navigation.navigate('Register');
            }}
          >
            Click me to register
          </Button>

          <Button
            shadow={2}
            onPress={() => {
              props.navigation.navigate('Home');
            }}
          >
            Click me to skip
          </Button>
        </VStack>
      </Center>
    </ScrollView>
  );
}

export default InfoServicio;
