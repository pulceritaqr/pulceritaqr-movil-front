import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Login from './Login';
import Prueba from './Prueba';

import Register from './Register';
import Dependientes from './Dependientes';
/* Secciones privadas */
import SideBar from './private/SideBar';
import PruebaInterna from './private/PruebaInterna';
import Configuracion from './private/Configuracion/Configuracion';
import MiInfo from './private/MiInfo/MiInfoForm';
import Payment from './private/Payment/PaymentOpt';
import PaymentForm from './private/Payment/PaymentForm';
import PersonaForm from './private/Personas/PersonaForm';
import PersonasLista from './private/Personas/PersonasLista';
import SuscripcionActiva from './private/Suscripcion/SuscripcionActiva';
import Planes from './private/Suscripcion/Planes';
import EnvioForm from './private/Soporte/EnvioForm';
import Soporte from './private/Soporte/Soporte';
/* Secciones privadas incio */
import InfoServicio from './../pages/Inicio/InfoServicio';
import PlanFamiliar from './../pages/Inicio/PlanFamiliar';
import PlanGrupal from './../pages/Inicio/PlanGrupal';
import InfoEnvios from './../pages/Inicio/InfoEnvios';

import authController from '../controllers/authController';
import { Button } from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Tab = createBottomTabNavigator();

function Main() {
  const [user, setUser] = useState(null);

  useEffect(() => {
    const getAuth = async () => {
      const userAuth = await authController.authUser();
      setUser(userAuth);
    };
    getAuth();
  }, []);

  function authLogout() {
    authController.authLogout();
    setUser(null);
  }
  const Stack = createNativeStackNavigator();

  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName='InfoServicio'
        screenOptions={{
          headerShown: false,
        }}
      >
        <Stack.Screen
          name='Login'
          options={{
            title: 'Login',
          }}
          component={Login}
        />
        <Stack.Screen
          name='Prueba'
          options={{
            title: 'Prueba',
          }}
          component={Prueba}
        />
        <Stack.Screen
          name='Register'
          options={{
            title: 'Registro',
          }}
          component={Register}
        />
        <Stack.Screen
          name='Home'
          options={{
            title: 'Home',
          }}
          component={SideBar}
        />
        <Stack.Screen
          name='PruebaInterna'
          options={{
            title: 'PruebaInterna',
          }}
          component={PruebaInterna}
        />
        <Stack.Screen
          name='Configuracion'
          options={{
            title: 'Configuración',
          }}
          component={Configuracion}
        />
        <Stack.Screen
          name='MiInfo'
          options={{
            title: 'Mi Información',
          }}
          component={MiInfo}
        />
        <Stack.Screen
          name='Payment'
          options={{
            title: 'Payment',
          }}
          component={Payment}
        />

        <Stack.Screen
          name='PaymentForm'
          options={{
            title: 'PaymentForm',
          }}
          component={PaymentForm}
        />
        <Stack.Screen
          name='PersonaForm'
          options={{
            title: 'none',
          }}
          component={PersonaForm}
        />
        <Stack.Screen
          name='PersonasLista'
          options={{
            title: 'none',
          }}
          component={PersonasLista}
        />

        <Stack.Screen
          name='Soporte'
          options={{
            title: 'Soporte',
          }}
          component={Soporte}
        />
        <Stack.Screen
          name='EnvioForm'
          options={{
            title: 'Mis datos de envio',
          }}
          component={EnvioForm}
        />
        <Stack.Screen
          name='SuscripcionActiva'
          options={{
            title: 'Mi Suscripción',
          }}
          component={SuscripcionActiva}
        />
        <Stack.Screen
          name='Planes'
          options={{
            title: 'Suscripción',
          }}
          component={Planes}
        />
        <Stack.Screen
          name='InfoServicio'
          options={{
            title: 'Información del Servicio',
          }}
          component={InfoServicio}
        />
        <Stack.Screen
          name='PlanFamiliar'
          options={{
            title: 'Plan Familiar',
          }}
          component={PlanFamiliar}
        />
        <Stack.Screen
          name='PlanGrupal'
          options={{
            title: 'Plan Grupal',
          }}
          component={PlanGrupal}
        />
        <Stack.Screen
          name='InfoEnvios'
          options={{
            title: 'Información sobre envios',
          }}
          component={InfoEnvios}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Main;
