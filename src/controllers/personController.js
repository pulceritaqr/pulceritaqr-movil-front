import BASE_URL_BACKEND from "../config/baseUrl.js";
import AsyncStorage from "@react-native-async-storage/async-storage";

const addPerson = async (gender, name, ap1, ap2, address, specialc) => {
  const user = JSON.parse(await AsyncStorage.getItem("user"));

  try {
    const res = await fetch(BASE_URL_BACKEND + "api/add-dependent/" + user.id, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + user.token,
      },
      body: JSON.stringify({
        name: name,
        lastName: ap1,
        secondLastName: ap2,
        address: address,
        special_cares: specialc,
        gender: gender,
      }),
    });
    const r = await res.json();
    console.log("dataaaa", r.data);
    return r;
  } catch (err) {
    console.log(err);
    return err;
  }
};

const registerController = {
  addPerson,
};

export default registerController;
