import React from 'react';
import {
  Center,
  Box,
  Button,
  VStack,
  FormControl,
  Input,
  ScrollView,
  Spacer,
  Text,
  Flex,
} from 'native-base';
import TituloSencilloModulo from '../../../componentes/TituloSencilloModulo';
import SubtituloModulo from '../../../componentes/SubtituloModulo';
import DescripcionSencilla from '../../../componentes/DescripcionSencilla';

function PaymentForm() {
  return (
    <ScrollView w='100%' h='80'>
      <Center w='100%'>
        <Box safeArea p='2' py='8' w='90%' maxW='290'>
          <VStack space={3} mt='1'>
            <SubtituloModulo texto='Informacion del pago' />
            <FormControl>
              <FormControl.Label>Nombre del titular</FormControl.Label>
              <Input variant='outline' placeholder=' ' />
            </FormControl>
            <FormControl>
              <FormControl.Label>Número de tarjeta</FormControl.Label>
              <Input variant='outline' placeholder='0000 0000 0000 0000' />
            </FormControl>
            <FormControl>
              <FormControl.Label>Fecha de vencimiento</FormControl.Label>
              <Input variant='outline' placeholder='00/00' />
            </FormControl>
            <FormControl>
              <FormControl.Label>CVV</FormControl.Label>
              <Input variant='outline' placeholder='000' />
            </FormControl>

            <Spacer mt='0' mb='0' />
            <SubtituloModulo texto='Dirección de envio' />
            <DescripcionSencilla texto='Estado nombre, municipio nombre, colonia nombre, direccion detallada' />
            <Flex direction='row-reverse'>
              <Button
                variant='subtle'
                size='xs'
                height='10'
                width='30%'
                colorScheme='indigo'
              >
                Modificar
              </Button>
            </Flex>
            <Spacer mt='0' mb='0' />

            <SubtituloModulo texto='Detalle de pedido' />

            <Spacer mt='0' mb='0' />

            <Text>Número de pedido: 000000</Text>
            <Text>Fecha de pago: 00/00/00</Text>
            <Text>Metodo de pago: xxxxxxxx</Text>
            <Text>Total a pagar: $199.99</Text>

            <Button
              mt='2'
              colorScheme='indigo'
              onPress={() => console.log('pressed')}
            >
              Aceptar
            </Button>
          </VStack>
        </Box>
      </Center>
    </ScrollView>
  );
}

export default PaymentForm;
