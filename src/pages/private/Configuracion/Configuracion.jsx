import React from 'react';
import { Center, Box, ScrollView } from 'native-base';
import DescripcionChk from '../../../componentes/DescripcionChk';
import TituloModulo from '../../../componentes/TituloModulo';
import SubtituloModulo from '../../../componentes/SubtituloModulo';

function Configuracion() {
  return (
    <ScrollView w='100%' h='80'>
      <Center w='100%'>
        <Box safeArea p='2' py='8' w='90%' maxW='290'>
          <TituloModulo
            name='Configuración'
            colorBase='green.100'
            colorFondo='green.500'
            nombreIonicos='settings-outline'
          ></TituloModulo>
          <Box mt='5' mb='5' />
          <SubtituloModulo texto='Notificaciones' mt='5'></SubtituloModulo>
          <DescripcionChk
            titulo='Notificaciones en la pantalla de bloqueo'
            texto='Permitir notificaciones en la pantalla de bloqueo .'
          ></DescripcionChk>
          <DescripcionChk
            titulo='Nuevos lanzamientos'
            texto='Recibe notificaciones de nuevos lanzamientos.'
          ></DescripcionChk>
        </Box>
      </Center>
    </ScrollView>
  );
}

export default Configuracion;
