import React from 'react';
import {
  Center,
  Box,
  Button,
  VStack,
  Input,
  ScrollView,
  Spacer,
  Icon,
  Pressable,
  HStack,
  Text,
} from 'native-base';
import TituloSencilloModulo from '../../../componentes/TituloSencilloModulo';
import SubtituloModulo from '../../../componentes/SubtituloModulo';
import DescripcionSencilla from '../../../componentes/DescripcionSencilla';
import { Ionicons, FontAwesome } from '@expo/vector-icons';

function PaymentOpt(props) {
  return (
    <ScrollView w='100%' h='80'>
      <Center w='100%'>
        <Box safeArea p='2' py='8' w='90%' maxW='290'>
          <VStack space={3} mt='1'>
            <SubtituloModulo texto='Selecciona la forma de pago' mt='5' />
            <Pressable mt='2' mb='1'>
              {({ isHovered, isFocused, isPressed }) => {
                return (
                  <Box
                    maxW='96'
                    borderColor='coolGray.100'
                    bg={
                      isPressed
                        ? 'coolGray.200'
                        : isHovered
                        ? 'coolGray.200'
                        : 'coolGray.100'
                    }
                    p='2'
                    rounded='8'
                    style={{
                      transform: [
                        {
                          scale: isPressed ? 0.96 : 1,
                        },
                      ],
                    }}
                  >
                    <HStack space='3' alignItems='center'>
                      <Box
                        maxW='20'
                        rounded='lg'
                        overflow='hidden'
                        borderColor='coolGray.400'
                      >
                        <Box size={30}>
                          <Icon
                            as={FontAwesome}
                            name='paypal'
                            size='25'
                            alignSelf='center'
                            margin='auto'
                            color='blue.600'
                          />
                        </Box>
                      </Box>
                      <Text fontSize='md' fontWeight='medium'>
                        Paypal
                      </Text>
                    </HStack>
                  </Box>
                );
              }}
            </Pressable>

            <Pressable mt='2' mb='1'>
              {({ isHovered, isFocused, isPressed }) => {
                return (
                  <Box
                    maxW='96'
                    borderColor='coolGray.100'
                    bg={
                      isPressed
                        ? 'coolGray.200'
                        : isHovered
                        ? 'coolGray.200'
                        : 'coolGray.100'
                    }
                    p='2'
                    rounded='8'
                    style={{
                      transform: [
                        {
                          scale: isPressed ? 0.96 : 1,
                        },
                      ],
                    }}
                  >
                    <HStack space='3' alignItems='center'>
                      <Box
                        maxW='20'
                        rounded='lg'
                        overflow='hidden'
                        borderColor='coolGray.400'
                      >
                        <Box size={30}>
                          <Icon
                            as={Ionicons}
                            name='card-outline'
                            size='25'
                            alignSelf='center'
                            margin='auto'
                            color='orange.600'
                          />
                        </Box>
                      </Box>
                      <Text fontSize='md' fontWeight='medium'>
                        Tarjeta de Crédito/ Débito
                      </Text>
                    </HStack>
                  </Box>
                );
              }}
            </Pressable>

            <Pressable mt='2' mb='1'>
              {({ isHovered, isFocused, isPressed }) => {
                return (
                  <Box
                    maxW='96'
                    borderColor='coolGray.100'
                    bg={
                      isPressed
                        ? 'coolGray.200'
                        : isHovered
                        ? 'coolGray.200'
                        : 'coolGray.100'
                    }
                    p='2'
                    rounded='8'
                    style={{
                      transform: [
                        {
                          scale: isPressed ? 0.96 : 1,
                        },
                      ],
                    }}
                  >
                    <HStack space='3' alignItems='center'>
                      <Box
                        maxW='20'
                        rounded='lg'
                        overflow='hidden'
                        borderColor='coolGray.400'
                      >
                        <Box size={30}>
                          <Icon
                            as={Ionicons}
                            name='finger-print-outline'
                            size='25'
                            alignSelf='center'
                            margin='auto'
                            color='dark.300'
                          />
                        </Box>
                      </Box>
                      <Text fontSize='md' fontWeight='medium'>
                        Mercado pago
                      </Text>
                    </HStack>
                  </Box>
                );
              }}
            </Pressable>

            <Pressable mt='2' mb='1'>
              {({ isHovered, isFocused, isPressed }) => {
                return (
                  <Box
                    maxW='96'
                    borderColor='coolGray.100'
                    bg={
                      isPressed
                        ? 'coolGray.200'
                        : isHovered
                        ? 'coolGray.200'
                        : 'coolGray.100'
                    }
                    p='2'
                    rounded='8'
                    style={{
                      transform: [
                        {
                          scale: isPressed ? 0.96 : 1,
                        },
                      ],
                    }}
                  >
                    <HStack space='3' alignItems='center'>
                      <Box
                        maxW='20'
                        rounded='lg'
                        overflow='hidden'
                        borderColor='coolGray.400'
                      >
                        <Box size={30}>
                          <Icon
                            as={FontAwesome}
                            name='money'
                            size='25'
                            alignSelf='center'
                            margin='auto'
                            color='green.600'
                          />
                        </Box>
                      </Box>
                      <Text fontSize='md' fontWeight='medium'>
                        Efectivo
                      </Text>
                    </HStack>
                  </Box>
                );
              }}
            </Pressable>
          </VStack>
        </Box>
      </Center>
    </ScrollView>
  );
}

export default PaymentOpt;
