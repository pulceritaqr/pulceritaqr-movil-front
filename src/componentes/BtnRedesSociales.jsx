import React from 'react';
import { Button, Center, Box, HStack, Icon, ZStack } from 'native-base';
import { Ionicons, FontAwesome } from '@expo/vector-icons';

function BtnRedesSociales(props) {
  return (
    <>
      <Button size='sm' variant='ghost'>
        <Icon
          as={Ionicons}
          name={props.nombreIonicos}
          size='35'
          alignSelf='center'
          margin='auto'
          color={props.colorFondo}
        />
      </Button>
    </>
  );
}

export default BtnRedesSociales;
